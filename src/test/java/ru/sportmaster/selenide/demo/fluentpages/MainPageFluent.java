package ru.sportmaster.selenide.demo.fluentpages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class MainPageFluent extends BasePageFluent {
    String shoppingCartButtonXpathTempltate = "//img[@alt='%s']/ancestor::div[@class='product-preview__area-photo']/following-sibling::div[@class='product-preview__area-bottom']//button[text()='В корзину']";

    public MainPageFluent openPage() {
        open("https://demo.yookassa.ru/");

        return this;
    }

    public MainPageFluent clickCartIcon() {
        $x("//span[contains(@class, 'icon-cart')]").click();

        return this;
    }

    public MainPageFluent addItemToShoopingCart(String itemName) {
        SelenideElement squareItemPad = $x("//img[@alt='%s']".formatted(itemName));//xpath
        squareItemPad.hover();
        SelenideElement putSquareToShoppingCartButton = $x(shoppingCartButtonXpathTempltate.formatted(itemName));
        putSquareToShoppingCartButton.click();
        putSquareToShoppingCartButton.doubleClick();
        putSquareToShoppingCartButton.contextClick();
        putSquareToShoppingCartButton.scrollIntoView(true);

        return this;
    }
}
