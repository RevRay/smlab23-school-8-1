package ru.sportmaster.selenide.demo.fluentpages;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selenide.$x;

public class ShoppingCartPageFluent extends BasePageFluent {

    public void assertTotalPriceCountPresent() {
        $x("//span[contains(@class, 'total-amount')]").should(appear);
    }
}
