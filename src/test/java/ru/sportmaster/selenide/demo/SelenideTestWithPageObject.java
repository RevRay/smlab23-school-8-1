package ru.sportmaster.selenide.demo;

import org.testng.annotations.Test;
import ru.sportmaster.selenide.demo.pages.MainPage;
import ru.sportmaster.selenide.demo.pages.ShoppingCartPage;

public class SelenideTestWithPageObject {
    @Test
    public void test() {
        MainPage mainPage = new MainPage();

        //fluent / chain of invocations stream().map().filter().forEach()
        mainPage.openPage();
        mainPage.addItemToShoopingCart("Квадрат");
        mainPage.addItemToShoopingCart("Круг");
        mainPage.clickCartIcon();

        ShoppingCartPage shoppingCartPage = new ShoppingCartPage();

        shoppingCartPage.assertTotalPriceCountPresent();
    }
}
