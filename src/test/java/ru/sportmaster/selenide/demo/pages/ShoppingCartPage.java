package ru.sportmaster.selenide.demo.pages;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selenide.$x;

public class ShoppingCartPage extends BasePage{

    public void assertTotalPriceCountPresent() {
        $x("//span[contains(@class, 'total-amount')]").should(appear);
    }
}
