package ru.sportmaster.selenide.demo.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class MainPage extends BasePage{
    String shoppingCartButtonXpathTempltate = "//img[@alt='%s']/ancestor::div[@class='product-preview__area-photo']/following-sibling::div[@class='product-preview__area-bottom']//button[text()='В корзину']";

    public void openPage() {
        open("https://demo.yookassa.ru/");
    }

    public void clickCartIcon() {
        $x("//span[contains(@class, 'icon-cart')]").click();
    }

    public void addItemToShoopingCart(String itemName) {
        SelenideElement squareItemPad = $x("//img[@alt='%s']".formatted(itemName));//xpath
        squareItemPad.hover();
        SelenideElement putSquareToShoppingCartButton = $x(shoppingCartButtonXpathTempltate.formatted(itemName));
        putSquareToShoppingCartButton.click();
        putSquareToShoppingCartButton.doubleClick();
        putSquareToShoppingCartButton.contextClick();
        putSquareToShoppingCartButton.scrollIntoView(true);
    }
}
