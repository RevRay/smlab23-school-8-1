package ru.sportmaster.selenide.demo;

import com.codeborne.selenide.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest {

    String shoppingCartButtonXpathTempltate = "//img[@alt='%s']/ancestor::div[@class='product-preview__area-photo']/following-sibling::div[@class='product-preview__area-bottom']//button[text()='В корзину']";

    @Test
    public void test() {

        open("https://demo.yookassa.ru/");

        //driver.findElement(By.xpath("")) // implicitlyWait() / WebDriverWait (driver, 5000, 100)
        //driver.findElements(By.xpath(""))
        //кладем Квадрат в коризну
        SelenideElement squareItemPad = $x("//img[@alt='Квадрат']");//xpath
		squareItemPad.hover();
        SelenideElement putSquareToShoppingCartButton = $x(shoppingCartButtonXpathTempltate.formatted("Квадрат"));
		putSquareToShoppingCartButton.click();
		putSquareToShoppingCartButton.doubleClick();
		putSquareToShoppingCartButton.contextClick();
        putSquareToShoppingCartButton.scrollIntoView(true);


        putSquareToShoppingCartButton.sendKeys(""); //pass
        putSquareToShoppingCartButton.setValue(""); //pass


        //кладем Круг в коризну
        SelenideElement circleItemPad = $x("//img[@alt='Круг']");//xpath
		circleItemPad.hover();
        SelenideElement putCircleToShoppingCartButton = $x(shoppingCartButtonXpathTempltate.formatted("Круг"));
        putCircleToShoppingCartButton.click();
        SelenideElement putToShoppingCartButton = $x("//img[@alt='Круг']/ancestor::div[@class='product-preview__area-photo']/following-sibling::div[@class='product-preview__area-bottom']//button[text()='В корзину']");

        $x("//span[contains(@class, 'icon-cart')]").click();
        $x("//span[contains(@class, 'total-amount')]").should(appear);
//        $x("//span[contains(@class, 'total-amount')]").shouldBe(visible);
//        $x("//span[contains(@class, 'total-amount')]").shouldHave(attribute("",""));

//        sleep(2500);
    }

    @BeforeClass
    private void setUp() {
        Configuration.timeout = 5000;
        Configuration.browser = "firefox";
    }
}
