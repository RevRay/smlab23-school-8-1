package ru.sportmaster.selenide.demo;

import org.testng.annotations.Test;
import ru.sportmaster.selenide.demo.fluentpages.MainPageFluent;
import ru.sportmaster.selenide.demo.pages.MainPage;
import ru.sportmaster.selenide.demo.pages.ShoppingCartPage;

public class SelenideTestWithPageObjectWithFluent {
    @Test
    public void test() {
        MainPageFluent mainPage = new MainPageFluent();

        //fluent / chain of invocations stream().map().filter().forEach()
        mainPage
                .openPage()
                .addItemToShoopingCart("Квадрат")
        		.addItemToShoopingCart("Круг")
        		.clickCartIcon();

        ShoppingCartPage shoppingCartPage = new ShoppingCartPage();

        shoppingCartPage.assertTotalPriceCountPresent();
    }
}
